from datetime import datetime
from os import path

from flask import Flask, render_template, request, jsonify, session, send_file, safe_join, url_for, abort, redirect
from flask.ext.sqlalchemy import SQLAlchemy
from hashids import Hashids
from modules import duplicate, scale, rotate, merge_split

app = Flask(__name__)
UPLOAD_FOLDER = path.join(path.dirname(path.abspath(__file__)), "upload")
OUTPUT_FOLDER = path.join(UPLOAD_FOLDER, "output")
ALLOWED_EXTENSIONS = {'.pdf', '.png', '.bmp', '.jpg', '.jpeg'}
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///index.db'
db = SQLAlchemy(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'
debugger = app.logger.debug


def allowed_file(filename):
    return path.splitext(filename.lower())[1] in ALLOWED_EXTENSIONS


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/pdf.php', methods=['GET', 'POST'])
def pdf_tools():
    if request.method == 'POST':
        output_files = []

        if session['files'] is not None and request.form.get('val', '0') != '0':
            if request.form['cmd'] == 'merge':
                file_paths = [path.join(app.config['UPLOAD_FOLDER'], file['filename']) for file in session['files']]
                output_file = path.basename(
                    merge_split.merge_pdf(file_paths, app.config['OUTPUT_FOLDER'], request.form['val']))
                output_files = [url_for('.link_parser', filename=output_file, dp='download')]
                return jsonify({"files": output_files})

            for file in session['files']:
                file_path = path.join(app.config['UPLOAD_FOLDER'], file['filename'])
                if request.form['cmd'] == 'rotate':
                    output_file = path.basename(
                        rotate.pdf_rotation(file_path, app.config['OUTPUT_FOLDER'], request.form['val']))
                    output_files.append(url_for('.link_parser', filename=output_file, dp='download'))
                elif request.form['cmd'] == 'split':
                    output_file = [path.basename(file_name) for file_name in
                                   merge_split.split_pdf(file_path, app.config['OUTPUT_FOLDER'])]
                    output_files.append(
                        [url_for('.link_parser', filename=splited_file, dp='download') for splited_file in output_file])
                else:
                    output_files = []
            return jsonify({"files": output_files})

        return jsonify({"files": output_files})
    return render_template("pdf.html")


@app.route('/image.php', methods=['GET', 'POST'])
def image_tools():
    if request.method == 'POST':
        output_files = []

        if session['files'] is not None and request.form.get('val', '0') != '0':
            for file in session['files']:
                file_path = path.join(app.config['UPLOAD_FOLDER'], file['filename'])
                if request.form['cmd'] == 'option-percentage':
                    output_file = path.basename(
                        scale.percentage(file_path, app.config['OUTPUT_FOLDER'], request.form['val']))
                elif request.form['cmd'] == 'option-cut':
                    output_file = path.basename(
                        scale.cut(file_path, app.config['OUTPUT_FOLDER'], request.form['val']))
                elif request.form['cmd'] == 'rotate':
                    output_file = path.basename(
                        rotate.image_rotation(file_path, app.config['OUTPUT_FOLDER'], request.form['val']))
                else:
                    output_file = ''

                output_files.append(url_for('.link_parser', filename=output_file, dp='download'))

        return jsonify({"files": output_files})

    return render_template("images.html")


@app.route('/upload.php', methods=['POST'])
def upload():
    session['files'] = []
    files = request.files.getlist('images')
    for item in duplicate.save(files):
        item.update({'url': url_for('.link_parser', filename=item['filename'], dp='preview')})
        session['files'].append(item)
    return jsonify({"file": session['files']})


@app.route('/<dp>/<filename>', methods=['GET'])
def link_parser(filename, dp):
    to_be_download = filename
    if '..' in to_be_download or to_be_download.startswith('/'):
        abort(404)
    if to_be_download:
        if dp == 'download':
            return send_file(safe_join(app.config['OUTPUT_FOLDER'], to_be_download))
        elif dp in ['preview', 'transport']:
            return send_file(safe_join(app.config['UPLOAD_FOLDER'], to_be_download))
        else:
            abort(404)


@app.route('/transport.php', methods=['GET', 'POST'])
def transport():
    # debugger(request.query_string)
    # debugger(request.environ['REMOTE_ADDR'])
    # debugger(request.environ['HTTP_USER_AGENT'])
    return render_template('transport.html')


@app.route('/transport-upload.php', methods=['POST'])
def tr_upload():
    from modules.schema import Files

    session['files'] = []
    files = request.files.getlist('transport-files')
    saved_index = duplicate.save(files)
    saved_file = Files.query.filter_by(file_hash=saved_index[0]['md5sum']).first()
    hashids = Hashids()
    url = hashids.encode(saved_file.id)
    return jsonify({"status": url})


@app.route('/t', defaults={'file_index': None})
@app.route('/t/<file_index>')
def tr_download(file_index):
    if file_index is None:
        return render_template("getfiles.html")

    from modules.schema import Files

    hashids = Hashids()
    file_id = hashids.decode(file_index)
    if file_id:
        file = Files.query.get(file_id)
        if (datetime.utcnow() - file.cdate).days >= 5:
            return render_template("getfiles.html", expired=True)
        else:
            # return send_from_directory(app.config['UPLOAD_FOLDER'], file.filename)
            # return url_for(".link_parser", filename=file.filename, dp="transport")
            # return render_template(
            #  "download_redirect_page.html", link=url_for(".link_parser", filename=file.filename, dp="transport"))
            return redirect(url_for(".link_parser", filename=file.filename, dp="transport"))
    else:
        return render_template("getfiles.html", notfound=True), 404


@app.route('/sa/8964', methods=['POST', 'GET'])
def link_manager_api():
    hashids = Hashids()
    from modules.schema import Files
    from datetime import timedelta

    files_record = Files.query.all()
    files = [{"hashed_link": url_for(".tr_download", file_index=hashids.encode(file_record.id)),
              "filename": file_record.filename,
              "created_date": datetime.strftime(file_record.cdate + timedelta(hours=8), '%Y-%m-%d')} for file_record in
             files_record]
    # debugger(files)

    return jsonify({"files": files})


@app.route('/sa/1010')
def link_manager():
    return render_template('admin.html')


if __name__ == '__main__':
    app.run(debug=True, host="0", port=5001)
