import os

from tools_for_com import app

__author__ = 'zhuang'
from modules import schema


def init():
    schema.db.create_all()


def empty():
    schema.db.drop_all()


def empty_dir(target_path):
    files = os.listdir(target_path)
    for file in files:
        try:
            os.remove(os.path.join(target_path, file))
        except (PermissionError, IsADirectoryError):
            empty_dir(os.path.join(target_path, file))

    # os.mkdir(os.path.join(app.config['OUTPUT_FOLDER']))
    # try:
    #     os.mkdir(target_path, mode=0o777)
    # except OSError:
    #     try:
    #         os.mkdir(os.path.join(target_path, 'output'), mode=0o777)
    #     except OSError:
    #         pass

    print('Files in "{}" have been removed'.format(target_path))


def clean(table):
    result=table.query.delete()
    schema.db.session.commit()
    return result


def rebuild():
    empty_dir(app.config['UPLOAD_FOLDER'])
    empty()
    init()


def clean_weekly():
    db_clean = clean(schema.Files)
    empty_dir(app.config['UPLOAD_FOLDER'])
    return db_clean
