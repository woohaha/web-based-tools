"""
duplicate
"""
from hashlib import md5
from random import random
from os import path

from modules.secure_filename import secure_filename


def save(files, **kwargs):
    from modules.schema import Files
    from tools_for_com import app, db

    result = []

    for file in files:
        md5sum = hashfile(file)
        uploaded_file = Files.query.filter_by(file_hash=md5sum).first()
        if uploaded_file:
            filename = uploaded_file.filename
        else:
            filename, ext = path.splitext(file.filename)
            filename = str(int(random() * 100000)) + '_' + secure_filename(filename) + ext
            file.save(path.join(app.config['UPLOAD_FOLDER'], filename))
            file_record = Files(file_hash=md5sum, filename=filename, groupindex=kwargs.get('groupindex', None))
            db.session.add(file_record)
            db.session.commit()
        if kwargs.get('groupindex', None):
            result = kwargs['groupindex']
        else:
            result.append({'filename': filename, 'md5sum': md5sum})

    return result


def hashfile(afile, blocksize=65536):
    hasher = md5()
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    afile.seek(0)
    return hasher.hexdigest()


def fetch_files(groupindex):
    from modules.schema import Files

    filesname = [file['filename'] for file in Files.query.filter_by(groupindex=groupindex)]
    return filesname
