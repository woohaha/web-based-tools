import re


def secure_filename(s):
    """Ensure user submited string not contains '/' or muti '-'.
    Args:
        s: submited string
    Returns:
        replaced slash string
    """
    s = re.sub('[" "\/\--.]+', '-', s)
    s = re.sub(r':-', ':', s)
    s = re.sub(r'^-|-$', '', s)
    return s
