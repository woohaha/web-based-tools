from os import path

from PIL import Image

__author__ = 'zhuang'


def percentage(file_path, output_dir, percentage_val):
    if percentage_val.isdigit():
        percentage_val = int(percentage_val) / 100
    else:
        raise Exception('invalid input')
    if percentage_val == 0:
        raise Exception('invalid input')

    filename, ext = path.splitext(path.basename(file_path))
    ext = 'JPEG'
    output_file = path.join(output_dir, filename + '_resized.' + ext)
    im = Image.open(file_path).convert('RGB')
    w, h = im.size
    nw, nh = w * percentage_val, h * percentage_val
    im.thumbnail((nw, nh))
    im.save(output_file, ext)
    return output_file


def cut(file_path, output_dir, cut_val):
    if cut_val.isdigit():
        cut_val = int(cut_val)
    else:
        raise Exception('invalid input')
    if cut_val == 0:
        raise Exception('invalid input')

    filename, ext = path.splitext(path.basename(file_path))
    ext = 'JPEG'
    output_file = path.join(output_dir, filename + '_resized.' + ext)
    im = Image.open(file_path).convert('RGB')
    w, h = im.size
    if w >= h:
        nw = cut_val
        nh = nw * h / w
    else:
        nh = cut_val
        nw = nh * w / h

    im.thumbnail((nw, nh))
    im.save(output_file, ext)
    return output_file


def size(file):
    pass
