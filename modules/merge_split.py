from os import path

import PyPDF2

__author__ = 'zhuang'


def merge_pdf(file_paths, output_dir, *args):
    merge = PyPDF2.PdfFileMerger()
    output_filename = path.join(output_dir, path.basename(file_paths[0]) + '_' + 'merged' + '.pdf')
    for single_file in file_paths:
        with open(single_file, 'rb')as single_pdf:
            merge.append(PyPDF2.PdfFileReader(single_file))

    merge.write(output_filename)
    return output_filename


def split_pdf(file_path, output_dir, *args):
    filename, ext = path.splitext(path.basename(file_path))
    splited_files = []

    source_file = open(file_path, 'rb')
    pdf = PyPDF2.PdfFileReader(source_file)

    for pagenum in range(pdf.numPages):
        output = PyPDF2.PdfFileWriter()
        page = pdf.getPage(pagenum)
        output.addPage(page)
        output_filename = path.join(output_dir, filename + '_' + 'page_' + str(pagenum + 1) + '.pdf')
        output_file = open(output_filename, 'wb')
        output.write(output_file)
        output_file.close()
        splited_files.append(output_filename)

    source_file.close()
    return splited_files
