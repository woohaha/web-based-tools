def logger(func):
    from modules.schema import Usage

    def inner(*args, **kwargs):
        function_logger = Usage.query.filter_by(function=func).first()
        if function_logger is not None:
            function_logger.ping()
        else:
            function_logger = Usage(func)

        from tools_for_com import db

        db.session.add(function_logger)
        db.session.commit()
        return func(*args, **kwargs)

    return inner
