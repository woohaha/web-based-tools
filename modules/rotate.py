from os import path

from PIL import Image
import PyPDF2

__author__ = 'zhuang'


def image_rotation(file_path, output_dir, degree):
    if degree.isdigit():
        degree = int(degree)

    filename, ext = path.splitext(path.basename(file_path))
    ext = 'JPEG'
    output_file = path.join(output_dir, filename + '_rotated.' + ext)

    im = Image.open(file_path).convert('RGB')

    # if degree == '90':
    #     im.transpose(Image.ROTATE_90)
    # if degree == '180':
    #     im.transpose(Image.ROTATE_180)
    # if degree == '270':
    #     im.transpose(Image.ROTATE_270)

    im.rotate(degree)
    im.save(output_file, ext)
    return output_file


def pdf_rotation(file_path, output_dir, degree):
    if degree.isdigit():
        degree = int(degree)
    filename, ext = path.splitext(path.basename(file_path))
    output_filename = path.join(output_dir, filename + '_rotated.pdf')

    source_file = open(file_path, 'rb')
    pdf = PyPDF2.PdfFileReader(source_file)
    output = PyPDF2.PdfFileWriter()

    for pagenum in range(pdf.numPages):
        page = pdf.getPage(pagenum)
        page.rotateClockwise(degree)
        output.addPage(page)

    output_file = open(output_filename, 'wb')
    output.write(output_file)
    output_file.close()
    source_file.close()
    return output_filename
