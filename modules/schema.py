from datetime import datetime

from sqlalchemy import Column, Integer
from sqlalchemy import String, DateTime
from tools_for_com import db


class Files(db.Model):
    __tablename__ = 'files'
    # id = Column(String(32), primary_key=True, index=True)
    id = Column(Integer(), primary_key=True, index=True)
    filename = Column(String(), unique=True)
    file_hash = Column(String(32), unique=True)
    cdate = Column(DateTime(), index=True)

    # def __init__(self, id, filename, file_hash):
    def __init__(self, file_hash, filename, cdate=None, **kwargs):
        # self.id = id
        self.filename = filename
        self.file_hash = file_hash
        if cdate is None:
            self.cdate = datetime.utcnow()
        else:
            self.cdate = cdate

    def __repr__(self):
        return '<File {}>'.format(self.filename)
